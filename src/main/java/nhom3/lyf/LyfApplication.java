package nhom3.lyf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LyfApplication {

	public static void main(String[] args) {
		SpringApplication.run(LyfApplication.class, args);
	}

}
